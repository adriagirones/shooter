﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class SliderRunTo1 : MonoBehaviour
{
   //  public bool b=true;
	 private Slider slider;
	// public float speed=0.5f;

	public TextMeshProUGUI progress;

	float time;

    private void Awake()
    {
		slider = GetComponent<Slider>();
		time = 0f;
		slider.value = 0;
	}

	public void showCharge(float timeRecharge)
    {
		time = 0f;
		slider.value = 0;
		StartCoroutine(showChargeIE(timeRecharge));
	}

	IEnumerator showChargeIE(float timeRecharge)
    {
		while(time < timeRecharge)
        {
			time += Time.deltaTime;
			slider.value = ((time * 100) / timeRecharge)/100;
			UpdateProgress(slider.value);
			yield return new WaitForEndOfFrame();
		}
		yield return new WaitForSeconds(0.3f);
		this.gameObject.SetActive(false);
	}

	public void UpdateProgress(float content)
	{
		progress.text = Mathf.Round(content * 100) + "%";
	}

	/*

	void Update()
    {
		
		if(b)
		{
			time+=Time.deltaTime*speed;
			slider.value = time;
			
        if(time>1)
		{
					b=false;
			time=0;
		}
    }
	}*/


}
