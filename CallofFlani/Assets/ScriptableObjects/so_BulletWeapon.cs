﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class so_BulletWeapon : so_Weapon
{
    public int maxammo;
    public int ammo;
    public int maxmag;
    public int mag;
    public int nbullets;
    public float reloadtime;
    public GameObject flare;

}
