﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class so_Inventory : ScriptableObject
{
    public so_Weapon actualWeapon;
    public so_Weapon weapon1;
    public so_Weapon weapon2;
    public int money;
    public int hp;

    public bool changeWeapon()
    {
        if(actualWeapon == weapon1 && weapon2 != null)
        {
            actualWeapon = weapon2;
            return true;
        }
        else if(actualWeapon == weapon2 && weapon1 != null)
        {
            actualWeapon = weapon1;
            return true;
        }
        return false;
    }
}