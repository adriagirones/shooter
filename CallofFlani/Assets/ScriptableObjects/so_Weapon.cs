﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class so_Weapon : ScriptableObject
{
    public string weaponName;
    public float dmg;
    public float cadence;
    public float range;
    public float momentum;
    public int price;

}
