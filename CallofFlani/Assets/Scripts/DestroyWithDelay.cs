﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyWithDelay : MonoBehaviour
{
    [SerializeField]
    private float delay = 0.2f;
    void Start()
    {
        Destroy(this.gameObject,delay);
    }
}
