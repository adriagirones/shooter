﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_BuyWeapon : MonoBehaviour
{
    public so_Weapon weaponToSell;
    public so_Inventory m_Inventory;
    public scr_Player scr_player;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
            scr_ManagerUI.Instance.showPopUp(true, weaponToSell.weaponName, weaponToSell.price);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
            scr_ManagerUI.Instance.showPopUp(false, weaponToSell.weaponName, weaponToSell.price);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player" && Input.GetKeyDown(KeyCode.E))
        {
            if(m_Inventory.money >= weaponToSell.price)
            {
                if (m_Inventory.weapon1 == weaponToSell) {
                    ((so_BulletWeapon)m_Inventory.weapon1).ammo = ((so_BulletWeapon)m_Inventory.weapon1).maxammo;
                }
                else if(m_Inventory.weapon2 == weaponToSell)
                {
                    ((so_BulletWeapon)m_Inventory.weapon2).ammo = ((so_BulletWeapon)m_Inventory.weapon2).maxammo;
                }
                else
                {
                    StartCoroutine(buywep());
                }
 
                m_Inventory.money -= weaponToSell.price;
                


                print("Weapon changed, actual weapon: " + m_Inventory.actualWeapon);
            }
        }

    }

    IEnumerator buywep()
    {
        if (m_Inventory.actualWeapon)
        {
            scr_player.gameObject.GetComponent<scr_Kashoot>().reloadB = true;
            scr_player.weapons[m_Inventory.actualWeapon.weaponName].GetComponent<Animator>().SetInteger("st", 2);
            yield return new WaitForSeconds(0.8f);
            scr_player.weapons[m_Inventory.actualWeapon.weaponName].SetActive(false);
            scr_player.weapons[m_Inventory.actualWeapon.weaponName].GetComponent<Animator>().SetInteger("st", 3);

            print("Weapon to change:" + m_Inventory.actualWeapon);
            if (m_Inventory.actualWeapon == m_Inventory.weapon1)
            {
                m_Inventory.weapon1 = weaponToSell;
            }
            else if (m_Inventory.actualWeapon == m_Inventory.weapon2)
            {
                m_Inventory.weapon2 = weaponToSell;
            }
            m_Inventory.actualWeapon = weaponToSell;

            scr_player.weapons[m_Inventory.actualWeapon.weaponName].SetActive(true);
            scr_ManagerUI.Instance.UpdateUI();
            yield return new WaitForSeconds(0.8f);
            scr_player.gameObject.GetComponent<scr_Kashoot>().reloadB = false;
        }

    }
}
