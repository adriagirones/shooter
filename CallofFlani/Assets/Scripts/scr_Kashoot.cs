﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class scr_Kashoot : MonoBehaviour
{
    public Camera FPScamera;
    public so_Inventory inventory;
    private bool cadence = true;
    public bool reloadB = false;
    private scr_Player scr_player;
    public GameObject bloodsplash;
    // Start is called before the first frame update
    void Start()
    {
        scr_player = transform.GetComponent<scr_Player>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            if (!reloadB)
            {

                if (cadence)
                {
                    if (Ammocheck(inventory.actualWeapon))
                    {
                        Attack(inventory.actualWeapon);
                        StartCoroutine(AttackSpeed(inventory.actualWeapon));
                    }
                }
            }
        }
        else if (Input.GetKeyDown(KeyCode.R))
            StartCoroutine(reload(inventory.actualWeapon));
    }

    public void checkhit(RaycastHit hit, so_Weapon wep)
    {
        if (hit.transform.tag == "enemy")
        {
            print("HIT ");
            hit.transform.GetComponentInParent<scr_enemy>().hitEnemy(hit, wep);
            Instantiate(bloodsplash, hit.point, Quaternion.identity);
            inventory.money += 10;
            scr_ManagerUI.Instance.UpdateUI();
        }
    }

    private void Attack(so_Weapon wep)
    {
        RaycastHit hit;
        if (wep.GetType() == typeof(so_BulletWeapon))
        {
            print(scr_player.weapons[inventory.actualWeapon.weaponName].name);

            Instantiate(((so_BulletWeapon)inventory.actualWeapon).flare, scr_player.weapons[inventory.actualWeapon.weaponName].transform.GetChild(2).transform);

            switch (((so_BulletWeapon)wep).nbullets)
            {
                case 1:
                    if (Physics.Raycast(FPScamera.transform.position, FPScamera.transform.forward, out hit, wep.range))
                    {
                        Debug.DrawLine(FPScamera.transform.position, hit.point, Color.red, 5f);
                        checkhit(hit, wep);
                    }

                    break;
                case 2:
                    for (int i = 0; i < 2; i++)
                    {
                        Vector3 newForward = new Vector3(FPScamera.transform.forward.x + Random.Range(-0.02f, +0.02f), FPScamera.transform.forward.y + Random.Range(-0.02f, +0.02f), FPScamera.transform.forward.z + Random.Range(-0.02f, +0.02f));
                        if (Physics.Raycast(FPScamera.transform.position, newForward, out hit, wep.range))
                        {
                            Debug.DrawLine(FPScamera.transform.position, hit.point, Color.red, 5f);
                            checkhit(hit, wep);
                        }
                    }
                    break;
                case 4:
                    for (int i = 0; i < 4; i++)
                    {
                        Vector3 newForward = new Vector3(FPScamera.transform.forward.x + Random.Range(-0.03f, +0.03f), FPScamera.transform.forward.y + Random.Range(-0.03f, +0.03f), FPScamera.transform.forward.z + Random.Range(-0.03f, +0.03f));
                        if (Physics.Raycast(FPScamera.transform.position, newForward, out hit, wep.range))
                        {
                            Debug.DrawLine(FPScamera.transform.position, hit.point, Color.red, 5f);
                            checkhit(hit, wep);
                        }
                    }
                    break;
            }

            //StartCoroutine(KnockWeapon());
        }
        else if (wep.GetType() == typeof(so_MeleeWeapon))
        {
            print("bate");
            transform.GetComponent<scr_Player>().weapons[inventory.actualWeapon.weaponName].GetComponent<Animator>().SetTrigger("isAttacking");
            if (Physics.Raycast(FPScamera.transform.position, FPScamera.transform.forward, out hit, wep.range))
            {
                Debug.DrawLine(FPScamera.transform.position, hit.point, Color.red, 1f);
                print(hit);
                checkhit(hit, wep);
            }
        }
    }

    IEnumerator KnockWeapon() {
        this.gameObject.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().flag = true;
        yield return new WaitForSeconds(0.1f);
        this.gameObject.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().flag2 = true;
        yield return new WaitForSeconds(0.05f);
        this.gameObject.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().flag = false;
        this.gameObject.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().flag2 = false;
    }

    IEnumerator AttackSpeed(so_Weapon wep)
    {
        cadence = false;
        yield return new WaitForSeconds(wep.cadence);
        cadence = true;
    }
    public bool Ammocheck(so_Weapon wep)
    {
        if (wep.GetType() == typeof(so_BulletWeapon))
        {
            if (((so_BulletWeapon)wep).mag > 0)
            {
                ((so_BulletWeapon)wep).mag--;
                scr_ManagerUI.Instance.UpdateUI();
                return true;
            }
        }
        else if (wep.GetType() == typeof(so_MeleeWeapon))
            return true;
        return false;
    }

    public SliderRunTo1 slider;

    IEnumerator reload(so_Weapon wep)
    {
        if (((so_BulletWeapon)wep).maxmag != ((so_BulletWeapon)wep).mag)
        {
            if (((so_BulletWeapon)wep).ammo > 0)
            {
                reloadB = true;
                slider.gameObject.SetActive(true);
                slider.showCharge(((so_BulletWeapon)wep).reloadtime);
                transform.GetComponent<scr_Player>().weapons[inventory.actualWeapon.weaponName].GetComponent<Animator>().SetInteger("st", 2);
                yield return new WaitForSeconds(((so_BulletWeapon)wep).reloadtime);
                if (((so_BulletWeapon)wep).maxmag <= ((so_BulletWeapon)wep).ammo && ((so_BulletWeapon)wep).mag==0)
                {
                    ((so_BulletWeapon)wep).mag = ((so_BulletWeapon)wep).maxmag;
                    ((so_BulletWeapon)wep).ammo -= ((so_BulletWeapon)wep).maxmag;
                    scr_ManagerUI.Instance.UpdateUI();
                    //print("maxmagfull");
                }
                else if(((so_BulletWeapon)wep).mag < ((so_BulletWeapon)wep).maxmag && ((so_BulletWeapon)wep).ammo>0)
                {
                    int aux = ((so_BulletWeapon)wep).maxmag - ((so_BulletWeapon)wep).mag;
                    if (((so_BulletWeapon)wep).ammo >= aux)
                    {
                        ((so_BulletWeapon)wep).mag += aux;
                        ((so_BulletWeapon)wep).ammo -= aux;
                    }
                    else
                    {
                        ((so_BulletWeapon)wep).mag = ((so_BulletWeapon)wep).ammo;
                        ((so_BulletWeapon)wep).ammo = 0;
                    }
                    scr_ManagerUI.Instance.UpdateUI();
                    //print("mag casi");
                }
                transform.GetComponent<scr_Player>().weapons[inventory.actualWeapon.weaponName].GetComponent<Animator>().SetInteger("st", 3);
                yield return new WaitForSeconds(0.7f);
            }
        }
        reloadB = false;

    }
}
