﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class scr_ManagerUI : MonoBehaviour
{
    public static scr_ManagerUI Instance;

    public so_Inventory inventory;

    public GameObject ammoBox;
    public GameObject mainWeapon;
    public GameObject secondWeapon;
    public GameObject shields;
    public GameObject gameOver;
    public GameObject shop;
    public TextMeshProUGUI ammoText;
    public TextMeshProUGUI magText;
    public TextMeshProUGUI shopWeapon;
    public TextMeshProUGUI shopCost;
    public TextMeshProUGUI money;
    public TextMeshProUGUI waveText;
    public Transform originWave;
    public Transform destinationWave;

    void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        UpdateUI();
    }

    public void UpdateUI()
    {
        UpdateWeapon();
        if (inventory.actualWeapon.GetType() == typeof(so_BulletWeapon))
        {
            ammoBox.SetActive(true);
            ammoText.gameObject.SetActive(true);
            magText.gameObject.SetActive(true);

            if(((so_BulletWeapon)inventory.actualWeapon).mag < 10)
                magText.text = "0"+((so_BulletWeapon)inventory.actualWeapon).mag;
            else
                magText.text = "" + ((so_BulletWeapon)inventory.actualWeapon).mag;

            if (((so_BulletWeapon)inventory.actualWeapon).ammo < 10)
                ammoText.text = "00" + ((so_BulletWeapon)inventory.actualWeapon).ammo;
            else if (((so_BulletWeapon)inventory.actualWeapon).ammo < 100)
                ammoText.text = "0" + ((so_BulletWeapon)inventory.actualWeapon).ammo;
            else
                ammoText.text = "" + ((so_BulletWeapon)inventory.actualWeapon).ammo;


        }
        else
        {
            ammoText.gameObject.SetActive(false);
            magText.gameObject.SetActive(false);
            ammoBox.SetActive(false);
        }

        for(int i = 0; i < shields.transform.childCount; i++)
        {
            shields.transform.GetChild(i).gameObject.SetActive(false);
            if (i <= inventory.hp - 1)
                shields.transform.GetChild(i).gameObject.SetActive(true);
        }


        money.text = inventory.money+"";
    }

    public void UpdateWeapon()
    {
        for(int i = 0; i < mainWeapon.transform.childCount; i++)
        {
            mainWeapon.transform.GetChild(i).gameObject.SetActive(false);
            if ((inventory.weapon1 == inventory.actualWeapon) && (mainWeapon.transform.GetChild(i).gameObject.name == inventory.weapon1.weaponName))
                mainWeapon.transform.GetChild(i).gameObject.SetActive(true);
            else if ((inventory.weapon2 == inventory.actualWeapon) && (mainWeapon.transform.GetChild(i).gameObject.name == inventory.weapon2.weaponName))
                mainWeapon.transform.GetChild(i).gameObject.SetActive(true);
        }

        for (int i = 0; i < secondWeapon.transform.childCount; i++)
        {
            secondWeapon.transform.GetChild(i).gameObject.SetActive(false);
            if ((inventory.weapon1 != inventory.actualWeapon) && (secondWeapon.transform.GetChild(i).gameObject.name == inventory.weapon1.weaponName))
                secondWeapon.transform.GetChild(i).gameObject.SetActive(true);
            else if ((inventory.weapon2 != inventory.actualWeapon) && (secondWeapon.transform.GetChild(i).gameObject.name == inventory.weapon2.weaponName))
                secondWeapon.transform.GetChild(i).gameObject.SetActive(true);
        }
    }

    public void UpdateWave(int wave)
    {
        StartCoroutine(UpdateWaveIE(wave));
    }

    IEnumerator UpdateWaveIE(int wave)
    {
        waveText.transform.parent.gameObject.SetActive(true);
        waveText.text = "Wave " + wave;

        float t = 0f;
        Vector3 originL = originWave.GetComponent<RectTransform>().anchoredPosition;
        Vector3 destinationL = destinationWave.GetComponent<RectTransform>().anchoredPosition;

        while (t < 1)
        {
            waveText.transform.parent.GetComponent<RectTransform>().anchoredPosition = Vector3.Lerp(originL, destinationL, t);
            t += 0.5f * Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(2f);
        t = 1;

        while (t > 0)
        {
            waveText.transform.parent.GetComponent<RectTransform>().anchoredPosition = Vector3.Lerp(originL, destinationL, t);
            t -= 0.5f * Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        waveText.transform.parent.gameObject.SetActive(false);

    }

    public void showGameOver()
    {
        StartCoroutine(showGameOverIE());
    }

    IEnumerator showGameOverIE()
    {
        gameOver.SetActive(true);
        Color objectColor = gameOver.GetComponent<Image>().color;
        float fadeAmount;
        int fadeSpeed = 1;
        while (gameOver.GetComponent<Image>().color.a < 0.75)
        {
            fadeAmount = objectColor.a + (fadeSpeed * Time.deltaTime);
            objectColor = new Color(objectColor.r, objectColor.g, objectColor.b, fadeAmount);
            gameOver.GetComponent<Image>().color = objectColor;
            yield return null;
        }
        Time.timeScale = 0;
    }

    public void showPopUp(bool flag, string weaponName, int cost)
    {
        shop.SetActive(flag);
        if (flag)
        {
            shopWeapon.text = weaponName;
            shopCost.text = cost+"$";
        }
       
    }


}
