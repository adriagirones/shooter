﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class scr_Player : MonoBehaviour
{
    public so_Inventory inventory;
    private bool canHit = true;
    private bool canHeal = false;

    public GameObject blood;
    public List<GameObject> weaps;
    public Dictionary<string, GameObject> weapons = new Dictionary<string, GameObject>();
    public so_Weapon[] soWeapons;
    
    void Start()
    {
        foreach(so_Weapon weapon in soWeapons)
        {
            if (weapon.GetType() == typeof(so_BulletWeapon))
            {
                ((so_BulletWeapon)(weapon)).mag = ((so_BulletWeapon)(weapon)).maxmag;
                ((so_BulletWeapon)(weapon)).ammo = ((so_BulletWeapon)(weapon)).maxammo;
            }
        }

        inventory.money = 100;

        inventory.actualWeapon = inventory.weapon1 = soWeapons[Random.Range(0, soWeapons.Length)];
        do inventory.weapon2 = soWeapons[Random.Range(0, soWeapons.Length)];
            while (inventory.weapon2 == inventory.weapon1);
            

        foreach (GameObject wep in weaps)
        {
            weapons.Add(wep.name, wep);
        }

        if (inventory.weapon1 != null)
        {
            inventory.actualWeapon = inventory.weapon1;
            weapons[inventory.actualWeapon.weaponName].SetActive(true);
        }

        inventory.hp = 4;
        scr_ManagerUI.Instance.UpdateUI();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            StartCoroutine(changewep());
        }

        if(inventory.hp == 0 && Time.timeScale == 0)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                Time.timeScale = 1;
            }
        }
    }

    IEnumerator changewep()
    {
        if (inventory.weapon2)
        {
            transform.GetComponent<scr_Kashoot>().reloadB = true;
            weapons[inventory.actualWeapon.weaponName].GetComponent<Animator>().SetInteger("st", 2);
            yield return new WaitForSeconds(0.8f);
            weapons[inventory.actualWeapon.weaponName].SetActive(false);
            weapons[inventory.actualWeapon.weaponName].GetComponent<Animator>().SetInteger("st", 3);
        }
        if (inventory.changeWeapon())
        {
            weapons[inventory.actualWeapon.weaponName].SetActive(true);
            yield return new WaitForSeconds(0.8f);
            scr_ManagerUI.Instance.UpdateUI();
            transform.GetComponent<scr_Kashoot>().reloadB = false;
        }

    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "enemy")
        {
            if (canHit)
            {
                inventory.hp -= 1;
                scr_ManagerUI.Instance.UpdateUI();
                StartCoroutine(updateBlood());

                if (inventory.hp <= 0)
                {
                    this.gameObject.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().flag = true;
                    scr_ManagerUI.Instance.showGameOver();
                }

                canHit = false;
                canHeal = false;
                StopCoroutine(heal());
                StartCoroutine(invencibility());
            }
        }
    }

    IEnumerator invencibility()
    {
        yield return new WaitForSeconds(3);
        canHit = true;
        canHeal = true;
        StartCoroutine(heal());
        print("endinven");
    }

    IEnumerator heal()
    {
        yield return new WaitForSeconds(5);
        while (inventory.hp < 4 && canHeal)
        {
            print("healed");
            inventory.hp += 1;
            if (inventory.hp > 4)
            {
                inventory.hp = 4;
                canHeal = false;
            }
            scr_ManagerUI.Instance.UpdateUI();
            StartCoroutine(updateBlood());
            yield return new WaitForSeconds(5);
        }
    }

    IEnumerator updateBlood()
    {
        Color objectColor = blood.GetComponent<Image>().color;
        float fadeAmount;
        int fadeSpeed = 1;
        float damage = 0;

        switch (inventory.hp)
        {
            case 1:
                damage = 0.5f;
                break;
            case 2:
                damage = 0.3f;
                break;
            case 3:
                damage = 0.1f;
                break;
            case 4:
                damage = 0f;
                break;
        }

        if (blood.GetComponent<Image>().color.a < damage)
        {
            while (blood.GetComponent<Image>().color.a < damage)
            {
                fadeAmount = objectColor.a + (fadeSpeed * Time.deltaTime);
                objectColor = new Color(objectColor.r, objectColor.g, objectColor.b, fadeAmount);
                blood.GetComponent<Image>().color = objectColor;
                yield return null;
            }
        }
        else if (blood.GetComponent<Image>().color.a > damage)
        {
            while (blood.GetComponent<Image>().color.a > damage)
            {
                fadeAmount = objectColor.a - (fadeSpeed * Time.deltaTime);
                objectColor = new Color(objectColor.r, objectColor.g, objectColor.b, fadeAmount);
                blood.GetComponent<Image>().color = objectColor;
                yield return null;
            }
        }
    }

    public IEnumerator fadeBlack(int fadeSpeed = 1)
    {
        Color objectColor = blood.GetComponent<Image>().color;
        float fadeAmount;
        blood.gameObject.SetActive(true);

        while (blood.GetComponent<Image>().color.a < 0.5)
        {
            fadeAmount = objectColor.a + (fadeSpeed * Time.deltaTime);
            objectColor = new Color(objectColor.r, objectColor.g, objectColor.b, fadeAmount);
            blood.GetComponent<Image>().color = objectColor;
            yield return null;
        }
        yield return new WaitForSeconds(8);
        while (blood.GetComponent<Image>().color.a > 0)
        {
            fadeAmount = objectColor.a - (fadeSpeed * Time.deltaTime);
            objectColor = new Color(objectColor.r, objectColor.g, objectColor.b, fadeAmount);
            blood.GetComponent<Image>().color = objectColor;
            yield return null;
        }
        blood.gameObject.SetActive(false);

    }


}