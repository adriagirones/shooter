﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_Pool : MonoBehaviour
{
    public GameObject objPrefab;

    public int poolSize;

    private Queue<GameObject> objPool;

    public int availableZombies;

    private void Start()
    {
        objPool = new Queue<GameObject>();

        for (int i = 0; i < poolSize; i++)
        {
            GameObject newObj = Instantiate(objPrefab);
            objPool.Enqueue(newObj);
            newObj.SetActive(false);
        }

        availableZombies = poolSize;
    }

    //AGAFA UN GAMEOBJECT DE LA POOL
    public GameObject GetObjFromPool()
    {
        availableZombies--;
        GameObject newObj = objPool.Dequeue();
        newObj.SetActive(true);
        newObj.GetComponent<scr_enemy>().initZombie();
        return newObj;
    }

    //RETORNA UN GAMEOBJECT A LA POOL
    public void ReturnObjToPool(GameObject go)
    {
        availableZombies++;
        go.SetActive(false);
        objPool.Enqueue(go);
    }

}
