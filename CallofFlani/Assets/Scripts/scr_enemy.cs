﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;

public class scr_enemy : MonoBehaviour
{
    public float HP;
    public int velocity;
    scr_waveManager wm;

    List<Transform> childsInitTransform;
    private float initHP;

    public scr_PoolManager pool;

    public enum zombieType {
        DEFAULT,
        GIRL,
        DOCTOR,
        SUITED,
        POLICE
    }

    public zombieType type;

    private void Awake()
    {
        childsInitTransform = new List<Transform>();

        wm = GameObject.Find("WaveManager").GetComponent<scr_waveManager>();
        pool = GameObject.Find("ZombiePool").GetComponent<scr_PoolManager>();

        Transform[] childList = GetComponentsInChildren<Transform>();

        for (int i = 0; i < childList.Length; i++)
        {
            childsInitTransform.Add(childList[i]);
        }

        initHP = HP;
    }

    private void Update()
    {
        if (transform.GetChild(1).position.y < -15)
            DeathEnemy();
    }

    public void initZombie()
    {
        Transform[] childList = GetComponentsInChildren<Transform>();

        for (int i = 0; i < childsInitTransform.Count; i++)
        {
            childList[i].position = childsInitTransform[i].position;
            childList[i].localEulerAngles = childsInitTransform[i].localEulerAngles;
        }
        HP = initHP;
        this.gameObject.transform.GetChild(0).gameObject.SetActive(false);
        this.gameObject.transform.GetChild(1).gameObject.SetActive(true);
    }

    public void hitEnemy(RaycastHit hit, so_Weapon wep)
    { 
        transform.GetComponentInChildren<Rigidbody>().AddForceAtPosition(-hit.normal * wep.momentum, hit.point);
        HP -= wep.dmg * 100;
        print("ENEMY HP: "+HP);

        if (HP <= 0)
            DeathEnemy();
    }

    public void DeathEnemy()
    {
        print("enemigo muerto");
        GetComponent<NavMeshAgent>().enabled = false;
        wm.enemigosEnEscena.Remove(this.gameObject);
        this.gameObject.transform.GetChild(1).gameObject.SetActive(false);
        this.gameObject.transform.GetChild(0).gameObject.transform.position = this.gameObject.transform.GetChild(1).gameObject.transform.position;

        Transform[] child0List = this.gameObject.transform.GetChild(0).GetComponentsInChildren<Transform>();
        Transform[] child1List = this.gameObject.transform.GetChild(1).GetComponentsInChildren<Transform>();
        for (int i = 0; i < child1List.Length; i++)
        {
            child0List[i].transform.position = child1List[i].transform.position;
            child0List[i].transform.localEulerAngles = child1List[i].transform.localEulerAngles;
        }

        this.gameObject.transform.GetChild(0).gameObject.transform.localEulerAngles = this.gameObject.transform.GetChild(1).gameObject.transform.localEulerAngles;
        this.gameObject.transform.GetChild(0).gameObject.SetActive(true);
        StartCoroutine(RemoveEnemy());
    }

    IEnumerator RemoveEnemy()
    {
        yield return new WaitForSeconds(15f);
        pool.ReturnZombie(this.gameObject);
    }
  
}
