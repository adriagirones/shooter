﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_waveManager : MonoBehaviour
{
    public GameObject[] spawnPoints;
    public GameObject[] Doors;
    public GameObject[] prefabDefaultEnemy;
    public List<GameObject> enemigosEnEscena = new List<GameObject>();
    public GameObject pool;

    private int enemigosXSpawn = 6; //6
    private int spawnTimesInWave = 0;//1
    private int actualSpawnTimes = 0;
    private int actualWave;
    private int activeSpawns;

    // Start is called before the first frame update
    void Start()
    {
        actualWave = 0;
        activeSpawns = 3;
    }

    // Update is called once per frame
    void Update()
    {
        if ((actualSpawnTimes == 0 && enemigosEnEscena.Count == 0) || Input.GetKeyDown("k"))
        {
            nextWave();
            actualWave++;
            activateSpawnPoint();
            scr_ManagerUI.Instance.UpdateWave(actualWave);
        }
    }

    public void activateSpawnPoint()
    {
        switch (actualWave)
        {
            case 2:
                //BATHROOMS
                activeSpawns = 5;
                Doors[0].SetActive(false);
                Doors[1].SetActive(false);
                break;
            case 3:
                //INFIRMARY
                Doors[2].SetActive(false);
                Doors[3].SetActive(false);
                Doors[4].SetActive(false);
                activeSpawns = 6;
                
                break;
            case 4:
                //LIBRARY AND CAFE
                Doors[5].SetActive(false);
                Doors[6].SetActive(false);
                Doors[7].SetActive(false);
                Doors[8].SetActive(false);
                activeSpawns = 8;
                break;
            case 5:
                //EXTERIOR
                Doors[9].SetActive(false);
                Doors[10].SetActive(false);
                activeSpawns = 9;
                break;
            case 6:
                //POOL
                Doors[11].SetActive(false);
                Doors[12].SetActive(false);
                activeSpawns = 10;
                break;
        }
    }

    private void nextWave() {
        enemigosXSpawn += 2;
        if(spawnTimesInWave % 2 == 0)
            spawnTimesInWave += 1;
        actualSpawnTimes = spawnTimesInWave;
        print(enemigosXSpawn);
        StartCoroutine(waitToSpawn());
    }

    IEnumerator waitToSpawn() {
        print("waitToSpawn");
        int total = 0;
        while (true) {
            yield return new WaitForSeconds(10);
            if (actualSpawnTimes != 0)
            {
                for (int i = 0; i < enemigosXSpawn; i++)
                {
                    GameObject go = pool.GetComponent<scr_PoolManager>().GetZombie();
                    Transform spawn = spawnPoints[Random.Range(0, activeSpawns)].transform;
                    go.transform.position = spawn.position;
                    go.transform.localEulerAngles = spawn.localEulerAngles;
                    go.GetComponent<scr_ZombieAgent>().getAgent().Warp(go.transform.position);
                    enemigosEnEscena.Add(go);
                   // enemigosEnEscena.Add(Instantiate(prefabDefaultEnemy[Random.Range(0, prefabDefaultEnemy.Length)],
                   // spawnPoints[Random.Range(0, spawnPoints.Length)].transform));
                    yield return new WaitForSeconds(1.5f);
                    print("enemigo spawneado");
                    total++;
                }
                actualSpawnTimes--;
                print(actualSpawnTimes);
            }
            if (actualSpawnTimes <= 0) {
                break;
            }
        }
        print("total " + total);
    }
}
