﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_CameraLerp : MonoBehaviour
{
    public Transform target;
    public Transform target2;
    public float smooth;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = target.position;
        //Quaternion aux = Quaternion.Euler(target2.rotation.x, target.rotation.y, 0);
        transform.rotation = Quaternion.Lerp(transform.rotation, target2.rotation, smooth);

        print(transform.rotation);
        print(target.rotation);
    }
}
