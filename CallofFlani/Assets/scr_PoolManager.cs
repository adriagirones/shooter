﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_PoolManager : MonoBehaviour
{
    public scr_Pool poolGirl;
    public scr_Pool poolTrajeado;
    public scr_Pool poolMedico;
    public scr_Pool poolPolice;
    public scr_Pool poolDefault;

    List<scr_Pool> pools = new List<scr_Pool>();

    void Awake()
    {
        pools.Add(poolGirl);
        pools.Add(poolTrajeado);
        pools.Add(poolMedico);
        pools.Add(poolPolice);
        pools.Add(poolDefault);
    }

    public GameObject GetZombie()
    {
        List<scr_Pool> availablePool = new List<scr_Pool>();

        foreach(scr_Pool type in pools)
        {
            if (type.availableZombies > 0)
                availablePool.Add(type);
        }

        return availablePool[Random.Range(0, availablePool.Count)].GetObjFromPool();
    }

    public void ReturnZombie(GameObject zombie)
    {
        switch (zombie.GetComponent<scr_enemy>().type)
        {
            case scr_enemy.zombieType.DEFAULT:
                poolDefault.ReturnObjToPool(zombie);
                break;
            case scr_enemy.zombieType.GIRL:
                poolGirl.ReturnObjToPool(zombie);
                break;
            case scr_enemy.zombieType.DOCTOR:
                poolMedico.ReturnObjToPool(zombie);
                break;
            case scr_enemy.zombieType.SUITED:
                poolTrajeado.ReturnObjToPool(zombie);
                break;
            case scr_enemy.zombieType.POLICE:
                poolPolice.ReturnObjToPool(zombie);
                break;
        }
    }


}
