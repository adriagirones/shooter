﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class scr_ZombieAgent : MonoBehaviour
{
    NavMeshAgent agent;
    GameObject destination;

    // Start is called before the first frame update
    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        destination = GameObject.Find("CharacterController");
    }

    private void OnEnable()
    {
        agent.enabled = true;
    }

    private void OnDisable()
    {
        agent.enabled = false;
    }

    public NavMeshAgent getAgent()
    {
        return agent;
    }

    // Update is called once per frame
    void Update()
    {
        if(agent.isActiveAndEnabled)
            agent.destination = new Vector3(destination.transform.position.x, destination.transform.position.y, destination.transform.position.z);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            print("aaa");
        }
    }
}
