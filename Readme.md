# Controls
Controls per teclat:

| Tecles | Acció |
| ------ | ------ |
|A/W/S/D         |`Moure el personatge`  |
|Click Esquerre  |`Disparar/Atacar`            |
|Shift           |`Córrer`            |
|Q               |`Canvi d'arma`            |
|E               |`Comprar armes`|
|R               |`Recarregar munició`|

# Gameplay
Món post-apocalíptic on has de sobreviure en una escola matant a tots els zombies. El joc passa en una escola on en la primera ronda estaran totes les aules tancades. A mida que es vagi avançant de rondes, s'aniran obrin noves zones com: els lavabos, la infermeria, el menjador, la biblioteca, el pati exterior i la piscina.
A mida que van avançant les rondes, la dificultat va augmentat i el nombre de zombies va augmentant. Cada cop que es mata un zombie, el personatge guanya diners amb els que pot comprar noves armes i munició. Aquestes armes estan repartides per l'escola.

## Requisits
- 7 tipus diferents d'armes diferents: 6 armes de bales amb diferent munició, abast i número de bales; 1 arma de curt abast amb diferent rang.
- Particles per al dispar i als enemics.
- Ragdolls en els enemics un cop moren.
- Diferents scriptables objects per exemple en cada arma per guardar les seves dades.
- Enemics amb navmesh.
- Shaders com per exemple en l'aigua de la piscina.
- Pool d'enemics.

# Crèdits

_Alberto Rivera Barrero_
* **Gitlab**: [ajrivera](https://gitlab.com/ajrivera)

_Daniel García Hernández_
* **Gitlab**: [dabrelad](https://gitlab.com/dabrelad)

_Adrià Gironès Calzada_
* **Gitlab**: [adriagirones](https://gitlab.com/adriagirones)
